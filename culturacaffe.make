api = 2
core = 7.x

; Dependencies =================================================================

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.2"
projects[token][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[standard][version] = "7.15"
projects[standard][subdir] = "contrib"

projects[webform][version] = "3.18"
projects[webform][subdir] = "contrib"

projects[context][version] = "3.0-beta4"
projects[context][subdir] = "contrib"

projects[context_layouts][version] = "3.0-beta4"
projects[context_layouts][subdir] = "contrib"

projects[context_ui][version] = "3.0-beta4"
projects[context_ui][subdir] = "contrib"

projects[ctools][version] = "1.2"
projects[ctools][subdir] = "contrib"

projects[devel_generate][version] = "1.3"
projects[devel_generate][subdir] = "contrib"

projects[profiler_builder][version] = "1.0-alpha2+5-dev"
projects[profiler_builder][subdir] = "contrib"

projects[devel][version] = "1.3"
projects[devel][subdir] = "contrib"

projects[fences][version] = "1.0"
projects[fences][subdir] = "contrib"

projects[simplehtmldom][version] = "1.12"
projects[simplehtmldom][subdir] = "contrib"

projects[views_ui][version] = "3.5"
projects[views_ui][subdir] = "contrib"

projects[views][version] = "3.5"
projects[views][subdir] = "contrib"

projects[features][version] = "1.0"
projects[features][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc3"
projects[admin_menu][subdir] = "contrib"

projects[admin_menu_toolbar][version] = "3.0-rc3"
projects[admin_menu_toolbar][subdir] = "contrib"

